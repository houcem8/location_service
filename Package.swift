// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "LocationService",
    platforms: [.iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "LocationService",
            type: .dynamic,
            targets: ["LocationService"]),
        .library(
          name: "LocationLiveClient",
          type: .dynamic,
          targets: ["LocationLiveClient"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
    ],
    targets: [
        .target(
            name: "LocationService",
            dependencies: []), 
        .target(
            name: "LocationLiveClient",
            dependencies: ["LocationService"],
            path: "Sources/LocationLiveClient"),
        .testTarget(
            name: "LocationServiceTests",
            dependencies: ["LocationService"]),
    ]
)
